﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserConsole : MonoBehaviour
{
    [SerializeField]
    private Button _closeButton;
    void Start()
    {
        _closeButton.onClick.AddListener(CloseClick);
    }

    private void CloseClick()
    {
        gameObject.SetActive(false);
    }
}
