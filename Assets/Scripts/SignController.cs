﻿using Assets.Scripts.com;
using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignController : MonoBehaviour
{
    [SerializeField]
    private Button _connectButton;
    [SerializeField]
    private Button _leaveButton;
    [SerializeField]
    private InputField _nameIF;
    [SerializeField]
    private InputField _chatNameIF;
    [SerializeField]
    private InputField _chatVersionIF;

    void Start()
    {
        _connectButton.onClick.AddListener(ConnectClick);
        _leaveButton.onClick.AddListener(LeaveClick);
    }

    private void ConnectClick()
    {
        if (_nameIF.text != "" && _chatNameIF.text != "" && _chatVersionIF.text != "")
        {
            EventManager._CONNECT(new MyEventData(new ConnectData(_nameIF.text, _chatNameIF.text, _chatVersionIF.text)));
        }        
    }

    private void LeaveClick()
    {
        
        Debug.Log("ClickDisconnect");

        EventManager._DISCONNECT(new MyEventData(true));
    }

}
