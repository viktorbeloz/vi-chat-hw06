﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstMessage : MonoBehaviour
{
    [SerializeField]
    private Text _messageText;
    [SerializeField]
    private Text _dateText;

    public void Instantiate(string msg, string time)
    {
        _messageText.text = msg;
        _dateText.text = time;
    }
}
