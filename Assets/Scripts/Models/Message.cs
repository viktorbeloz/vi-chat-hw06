﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class Message
    {
        public string textMessage;
        public string dateTime;
        public Message(string msg, string dateT)
        {
            textMessage = msg;
            dateTime = dateT;
        }
    }

}
