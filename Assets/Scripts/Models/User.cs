﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class User
    {
        public Dictionary<string, List<Message>> messages;

        public string userName;
        public string password;

        public User(string usName, string pass)
        {
            userName = usName;
            password = pass;
            messages = new Dictionary<string, List<Message>>();
        }

        public void AddMsg(string key, string msg, string dateTime)
        {
            if (messages.ContainsKey(key))
            {
                messages[key].Add(new Message(msg,dateTime));
            }
            else
            {
                List<Message> a = new List<Message>();
                a.Add(new Message(msg,dateTime));
                messages.Add(key, a);
            }
        }
    }
}
