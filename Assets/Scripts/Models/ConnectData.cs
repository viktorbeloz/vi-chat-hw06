﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    class ConnectData
    {
        public string name { get; set; }
        public string chatName { get; set; }
        public string chatVersion { get; set; }

        public ConnectData(string name,string chatName, string chatVersion)
        {
            this.name = name;
            this.chatName = chatName;
            this.chatVersion = chatVersion;
        }
    }
}
