﻿using Assets.Scripts.com;
using Assets.Scripts.Models;
using ExitGames.Client.Photon;
using Photon.Chat;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatManager : MonoBehaviour, IChatClientListener
{
    [SerializeField]
    private Button _sendButton;
    [SerializeField]
    private InputField _messageIF;

    [SerializeField]
    private GameObject _myMsgPrefab;
    [SerializeField]
    private GameObject _userMsgPrefab;

    [SerializeField]
    private RectTransform _parent;

    [SerializeField]
    private GameObject _console;
    [SerializeField]
    private Text _consoleText;
    [SerializeField]
    private Button _consoleOpenButton;

    private string _chatName;
    private string _userName;

    private ChatClient _chat;
    private bool isConnected;


    private Data data = new Data();

    void Start()
    {
        EventManager._CONNECT += OnConn;
        EventManager._DISCONNECT += OnDisc;
        _chat = new ChatClient(this);

        _sendButton.onClick.AddListener(SendClick);
        _consoleOpenButton.onClick.AddListener(OpenConsoleClick);
    }

    private void Update()
    {
        if (_chat != null)
        {
            _chat.Service();
        }
    }
    private void OpenConsoleClick()
    {
        _console.SetActive(true);
    }
    private void OnConn(IData data)
    {
        if (isConnected is false)
        {
            try
            {
                ConnectData connectData = (ConnectData)((MyEventData)data).data;
                _chat.Connect(PhotonNetwork.PhotonServerSettings.AppID, connectData.chatVersion, new Photon.Chat.AuthenticationValues(connectData.name));
                isConnected = true;
                _userName = connectData.name;
                _chatName = connectData.chatName;
            }
            catch (Exception ex)
            {
                _consoleText.text += $"{ex.Message}\n";
            }
            
        }
    }
    private void OnDisc(IData data)
    {

        bool isDisconnect = (bool)((MyEventData)data).data;

        if (isDisconnect)
        {
            _parent.DetachChildren();
            isConnected = false;
            _chat.Disconnect();
        }
    }
    private void SendClick()
    {
        SendMessage($"{_messageIF.text}");
        _messageIF.text = "";
    }
    private bool SendMessage(string msg)
    {
        if (_messageIF.text != "")
        {
            _chat.PublishMessage(_chatName, msg);
            return true;
        }
        return false;
    }


    public void DebugReturn(DebugLevel level, string message)
    {
        Debug.Log(message);
    }

    public void OnChatStateChange(ChatState state)
    {
        Debug.Log(state.ToString());
    }

    public void OnConnected()
    {
        _consoleText.text += $"You connected to {_chatName}\n";
        _chat.Subscribe(new string[] { _chatName }, 20);
    }

    public void OnDisconnected()
    {
        _consoleText.text += $"You disconnected\n";
    }


    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        for (int i = 0; i < messages.Length; i++)
        {
            InstMessage iMsg;
            if (senders[i] == _userName)
            {
                iMsg = Instantiate(_myMsgPrefab, _parent).GetComponent<InstMessage>();
            }
            else
            {
                iMsg = Instantiate(_userMsgPrefab, _parent).GetComponent<InstMessage>();
            }
            DateTime time = DateTime.Now;
            iMsg.Instantiate($"{senders[i].ToString()}: {messages[i].ToString()}", time.ToString());

        }

    }

    public void OnPrivateMessage(string sender, object message, string channelName)
    {
        throw new System.NotImplementedException();
    }

    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
        throw new System.NotImplementedException();
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {
        _consoleText.text += $"You subscribet {_chatName}\n";
    }

    public void OnUnsubscribed(string[] channels)
    {
        _consoleText.text += $"You unsubscribet {_chatName}\n";
    }

    public void OnUserSubscribed(string channel, string user)
    {
        _consoleText.text += $"{user} subscribet {channel}\n";
    }

    public void OnUserUnsubscribed(string channel, string user)
    {
        _consoleText.text += $"{user} unsubscribet {channel}\n";
    }

}
