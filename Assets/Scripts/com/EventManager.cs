﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.com
{
    interface IEvent
    {

    }
    public class EventManager
    {
        public delegate void CONNECT(IData data);
        public delegate void DISCONNECT(IData data);

        static public CONNECT _CONNECT;
        static public DISCONNECT _DISCONNECT;

    }
}
