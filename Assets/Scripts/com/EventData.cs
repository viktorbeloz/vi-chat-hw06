﻿using Assets.Scripts.com;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.com
{
    public interface IData
    {

    }
    public class MyEventData : IData
    {
        public object data { get; set; }

        public MyEventData(object _data)
        {
            data = _data;
        }

    }
}
